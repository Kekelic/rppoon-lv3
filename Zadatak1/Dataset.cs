﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class Dataset : Prototype
    {
        private List<List<string>> data;

        public Dataset()
        {
            this.data = new List<List<string>>();
        }

        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }

        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }

        public void ClearData()
        {
            this.data.Clear();
        }

        public Dataset (Dataset dataset)
        {
            //This class requires deep copying because it uses lists
            this.data = new List<List<string>>();
            foreach (List<string> rows in dataset.data)
            {   
                foreach (string word in rows)
                {
                    rows.Add(word);
                }
               this.data.Add(rows);
            }
            
        }

        public Prototype Clone() => new Dataset(this);

    }
}

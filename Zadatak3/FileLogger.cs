﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    //If we use this class as a singleton, we will always write to the same file unless we change it over the setter
    class FileLogger
    {
        private static FileLogger instance;
        private string filePath;

        public FileLogger()
        {
            this.filePath = "lv3_zad3";
        }

        public static FileLogger GetInstance()
        {
            if(instance == null)
            {
                instance = new FileLogger();        
            }
            return instance;
        }

        public void setFilePath(string filePath)
        {
            this.filePath = filePath;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(message);
            }
        }
    }
}

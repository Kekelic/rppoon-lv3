﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime currentTime = DateTime.Now; 
            Category category = Category.INFO;          
            ConsoleColor color = ConsoleColor.Green;

            ConsoleNotification notification = new ConsoleNotification("Stjepan", "lv3", "zadataka 4", currentTime, category, color);

            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(notification);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class RandomGeneratorMatrix
    {
        private static RandomGeneratorMatrix instance;
        private Random generator;

        private RandomGeneratorMatrix()
        {
            this.generator = new Random();
        }

        public static RandomGeneratorMatrix GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomGeneratorMatrix();
            }
            return instance;
        }

        public double[,] GenerateDouble(int rows, int colomns)
        {
            //this method has two responsibilities, fills the matrix and returns the filled matrix (violation of the SRP)
            double[,] matrix = new double[rows, colomns];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < colomns; j++)
                {
                    matrix[i, j] = this.generator.NextDouble();
                }
            return matrix;
        }

    }
}

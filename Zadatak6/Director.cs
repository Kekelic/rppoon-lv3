﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Director
    {
        public void ConstructERROR(IBuilder builder, string author)
        {
            builder.SetAuthor(author).SetTitle("lv3").SetText("You have notification").SetTime(DateTime.Now).SetLevel(Category.ERROR).SetColor(ConsoleColor.Red);
        }

        public void ConstructALERT(IBuilder builder, string author)
        {
            builder.SetAuthor(author).SetTitle("lv3").SetText("You have notification").SetTime(DateTime.Now).SetLevel(Category.ALERT).SetColor(ConsoleColor.Yellow);
        }

        public void ConstructINFO(IBuilder builder, string author)
        {
            builder.SetAuthor(author).SetTitle("lv3").SetText("You have notification").SetTime(DateTime.Now).SetLevel(Category.INFO).SetColor(ConsoleColor.Blue);
        }
    }
}
